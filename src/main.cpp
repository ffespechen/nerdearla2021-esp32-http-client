#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

// Credenciales wifi
const char *ssid = "************";
const char *password = "************";

// Server al que me conectaré
const char *serverName = "http://192.168.0.109:5000/recibir-json";

// Delay entre lectura y publicación de datos
unsigned long pausa_entre_publicaciones = 10000;

// Pines del Joystick
const int pinX = 34;
const int pinY = 35;

void setup()
{
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Conectando");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Conectado a la red WiFi: ");
  Serial.println(WiFi.localIP());

  Serial.println("Cada 10 segundos se tomarán lecturas y se publicarán en el servidor");
}

void loop()
{

  delay(pausa_entre_publicaciones);
  // Verificamos si aún estamos conectados
  if (WiFi.status() == WL_CONNECTED)
  {
    WiFiClient client;
    HTTPClient http;

    // Inicio el cliente http
    http.begin(client, serverName);

    // Leo los datos y los envío como JSON
    DynamicJsonDocument misDatos(1024);
    char salida[1024];

    misDatos["X"] = analogRead(pinX);
    misDatos["Y"] = analogRead(pinY);

    serializeJsonPretty(misDatos, salida);
    Serial.println(salida);

    // Envío la información en formato JSON
    http.addHeader("Content-Type", "application/json");

    int httpResponseCode = http.POST(salida);

    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);

    // Libero los recursos
    http.end();
  }
  else
  {
    Serial.println("Desconectado del WiFi");
  }
}